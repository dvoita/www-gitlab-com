require 'spec_helper'
require_relative '../../helpers/product_category_helpers'

describe ProductCategoryHelpers do
  let(:klass) do
    Class.new do
      include ProductCategoryHelpers

      def initialize(stages)
        @stages = stages
      end

      def data
        Middleman::Util::EnhancedHash.new(stages: { stages: @stages })
      end
    end
  end

  let(:stages) do
    {
      manage: {
        groups: {
          access: {
            name: 'Access',
            categories: %w[authentication_and_authorization subgroups users]
          }
        }
      },
      plan: {
        groups: {
          project_management: {
            name: 'Project Management',
            categories: %w[issue_tracking boards time_tracking jira_importer projects]
          },
          no_categories: {
            name: 'No categories'
          }
        }
      },
      stage_no_groups: {}
    }
  end

  subject { klass.new(stages) }

  describe '#category_groups' do
    it 'returns a hash mapping category slugs to groups' do
      expect(subject.category_groups)
        .to include('authentication_and_authorization' => stages.dig(:manage, :groups, :access),
                    'boards' => stages.dig(:plan, :groups, :project_management))
    end
  end

  describe '#category_group_link' do
    it 'returns "Not owned" when the category has no group' do
      expect(subject.category_group_link('no_group')).to eq('Not owned')
    end

    it 'returns a link to the group when the category has a group' do
      expect(subject.category_group_link('projects')).to eq('[Project Management](#project-management-group)')
    end
  end
end
